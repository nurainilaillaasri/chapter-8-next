import React from 'react';
import Image from 'next/image';
import styles from '../styles/PakeServerSideRendering.module.css';
import Navbar from '../components/navbar';

const photo = ({ posts }) => {
  return (
    <div>
      <Navbar />
      {posts.map(({ id, title, thumbnailUrl }) => {
        console.log(thumbnailUrl);
        return (
          <a key={id}>
            <h2>{id}</h2>
            <p>{title}</p>
            <img src={thumbnailUrl} alt="" />
          </a>
        );
      })}
    </div>
  );
};

export default photo;

export async function getServerSideProps() {
  const response = await fetch('https://jsonplaceholder.typicode.com/photos');
  let posts = await response.json();
  posts = posts.slice(0, 10);

  return {
    props: {
      posts,
    },
  };
}
