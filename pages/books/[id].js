import { useRouter } from 'next/router';

export default function Hai() {
  const router = useRouter();
  const { id } = router.query;
  return (
    <>
      <h1>Book id : {id}</h1>
    </>
  );
}
