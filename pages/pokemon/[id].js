import Head from "next/head";
import Image from "next/image";
import styles from "../../styles/id.module.css";

export default function Contoh({ firmness, flavors, growth_time, id, item }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>{firmness.name}</title>
        <meta name="description" content={firmness.name} />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>{firmness.name}</h1>
        <h3 className={styles.title}>{firmness.url}</h3>

        <h2>flavor list</h2>
        <p className={styles.description}>{flavors[0].flavor.name}</p>
        <p className={styles.description}>{flavors[0].name}</p>

        <p className={styles.description}> growth_time : {growth_time} </p>
        <p className={styles.description}> id : {id} </p>
        <p className={styles.description}> id : {item.name} </p>

      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{" "}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  );
}

export async function getStaticPaths() {
  const response = await fetch("https://pokeapi.co/api/v2/berry/");
  const posts = await response.json();

  const paths = posts.results.map(({ id }, index) => ({
    params: {
      id: (index + 1).toString(),
    },
  }));

  return {
    paths,
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  console.log(params.id)
  const response = await fetch(
    "https://pokeapi.co/api/v2/berry/" + params.id
  );
  const post = await response.json();
  console.log(post)

  return {
    props: {
      ...post,
    },
  };
}